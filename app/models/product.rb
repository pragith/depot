class Product < ActiveRecord::Base

	default_scope :order => 'title'
	has_many :line_items
	before_destroy :ensure_not_referenced_by_any_line_item

	#Validation stuff
	validates :title, :description, :presence =>true
	validates :price, :numericality => {:greater_than_or_equal_to => 0.01}
	validates :title, :uniqueness => true
	validates :image_url, :format => {:with => %r{\.(gif|jpg|png)$}i, :message => 'must be a gif, jpg or png.'}
	
	private
	
		#Ensure that there are no line items referencing this product
		def ensure_not_referenced_by_any_line_item
			if line_items.empty?
				return true
			else
				error.add(:base, 'Lines items present')
				return false
			end
		end				  
end